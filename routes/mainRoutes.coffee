Router.route "/", (->
  	@render "home"
  	#SEO.set title: "Home - " + Meteor.App.NAME
  	return
),
	path: '/',
	template: 'home',
	data: ->
		challenges: Challenges.find(),
		total_questions: 10,
		total_time: 30,
		loggedInUser: Meteor.user()
	waitOn: -> [
			Meteor.subscribe "challenges"
		]
	onBeforeAction: ->
		#console.log "onBeforeAction"
		@next()
	action: ->
		if @ready()
			@render()
		else @render('loading')
