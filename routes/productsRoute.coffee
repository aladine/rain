Router.route 'products.index',
	path : '/products'
	template: 'products'
	data: ->
		products: Products.find()
	waitOn: ->
		[
			Meteor.subscribe 'products'
			Meteor.subscribe 'categories'
		]
	onBeforeAction: ->
		@next()
	action: ->
		if @ready()
			@render()
		else
			@render 'loading'

Router.route 'products.view',
	path: 'products/:_id/view'
	template: 'products'
	data: ->
		products: Products.find _id: @params._id
	waitOn: ->
		Meteor.subscribe 'products'
	onBeforeAction: ->
		@next()
	action: ->
		if @ready()
			@render()
		else
			@render('loading')
