Router.route "challenge.show",
	path: '/challenge/:_id',
	template: 'challenge',
	data: ->
		fn = (str)->
			if (str+"").length is 1 then "0"+str else str

		challenge = Challenges.find _id: @params._id
		questions = Questions.find challenge_id: @params._id
		t = Session.get('time') - Session.get('start_time')
		s = parseInt(t/1000)
		liveData =
			challenge: challenge
			questions: questions
			current_question: questions.fetch()[Session.get("question_number")]
			time_left: fn(parseInt(s/60,10)) + ":" + fn(s%60)

		liveData

	waitOn: -> [
			Meteor.subscribe "challenges"
			Meteor.subscribe "questions"
		]

	onBeforeAction: ->
		#console.log "onBeforeAction"
		Session.set "question_number", 0
		Session.set "start_time", new Date()
		Meteor.setInterval (->
		  	Session.set "time", new Date()
		  	return
		), 1000
		@next()
	action: ->
		if @ready()
			@render()
		else @render('loading')
