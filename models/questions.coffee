class @Questions extends Minimongoid
	@_collection: new Mongo.Collection('questions',
		schema: new SimpleSchema(
			title:
				type: String

			content:
				type: String

			createdAt:
				type: Date,
				denyUpdate: true
		)
	)

if Meteor.isServer
	Questions._collection.allow(
		insert : -> true

		update : -> true

		remove : -> true
	)
