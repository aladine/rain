class @Products extends Minimongoid
	@_collection: new Mongo.Collection('products',
		schema: new SimpleSchema(
			name:
				type: String

			category:
				type: @Categories

			createdAt:
				type: Date,
				denyUpdate: true
		)
	)
	@embeded_many:[
		{'name': 'categories'}
	]
	# @before_create: (attr) ->
	#   attr.name = _.titleize(attr.name)
	#   attr


if Meteor.isServer
	Products._collection.allow(
		insert : -> true

		update : -> true

		remove : -> true
	)


