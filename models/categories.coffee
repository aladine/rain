class @Categories extends Minimongoid
	@_collection: new Mongo.Collection('categories',
		schema: new SimpleSchema(
			title:
				type: String

			content:
				type: String

			createdAt:
				type: Date,
				denyUpdate: true
		)
	)

if Meteor.isServer
	Categories._collection.allow(
		insert : -> true

		update : -> true

		remove : -> true
	)
