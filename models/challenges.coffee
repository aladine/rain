class @Challenges extends Minimongoid
	@_collection: new Meteor.Collection('challenges',
		schema: new SimpleSchema(
			title:
				type: String
				index: 1

			name:
				type: String

			createdAt:
				type: Date,
				denyUpdate: true
		)
	)
	@has_many:[
		{
			name: 'questions'
			foreign_key: 'challenge_id'
		}
	]
	@defaults:
  		name: ''
  		createdAt: new Date().getTime()

if Meteor.isServer
	Challenges._collection.allow(
		insert : -> true

		update : -> true

		remove : -> true
	)
