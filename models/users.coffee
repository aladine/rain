class @Users extends Minimongoid
	@_collection: Meteor.users
	email: ->
    	if (@emails and @emails.length) then @emails[0].address else ''

if Meteor.isServer
	Users._collection.allow(
		insert : -> true

		update : -> true

		remove : -> true
	)


