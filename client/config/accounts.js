Meteor.startup(function(){
  	// process.env.MAIL_URL = 'smtp://postmaster';
});

AccountsTemplates.configure({
	    // Behaviour
	    confirmPassword: true,
	    enablePasswordChange: true,
	    forbidClientAccountCreation: false,
	    overrideLoginErrors: true,
	    sendVerificationEmail: false,

	    // Appearance
	    showAddRemoveServices: true,
	    showForgotPasswordLink: true,
	    showLabels: true,
	    showPlaceholders: true,

	    // Client-side Validation
	    continuousValidation: false,
	    negativeFeedback: false,
	    negativeValidation: true,
	    positiveValidation: true,
	    positiveFeedback: true,
	    showValidating: true,

	    // Privacy Policy and Terms of Use
	    privacyUrl: '/home/privacy',
	    termsUrl: '/home/terms-of-use',

	    // Redirects
	    homeRoutePath: '/home',
	    redirectTimeout: 4000,

	    // Texts
	    texts: {
	      button: {
	           	changePwd: "Change",
	          	enrollAccount: "Enroll Text",
	          	forgotPwd: "Forget meh",
	          	resetPwd: "Reset",
	          	signIn: "Let's go",
	          	signUp: "Go"
	      },
	      socialSignUp: "Register",
	      socialIcons: {
	          "meteor-developer": "fa fa-rocket"
	      },
	      title: {
	          forgotPwd: "Recover Your Passwod"
	      },
	    },
	});

	AccountsTemplates.addFields(
		[
			{
			    _id: "name",
			    type: "text",
			    required: true, 
			    displayName: "Name"
			},
			{
				 _id: "gender",
			    type: "select",
			    displayName: "Gender",
			    select:[
			    	{
			    		'text':'Male',
			    		'value': 'male'
			    	},
			    	{
			    		'text':'Female',
			    		'value': 'female'
			    	}
			    ]
			}
		]
	);
	
	AccountsTemplates.configureRoute('forgotPwd', {
	    name: 'home',
	    path: '/forgotPwd',
	    template: 'forgetPwd',
	    // layoutTemplate: 'home',
	    redirect: '/',
	});