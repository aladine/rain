function loadFixture(fixtures, collection) {
  var i;
  collection.remove({});
  for (i = 0; i < fixtures.length; i+= 1) {
    collection.insert(fixtures[i]);
  }
}

Meteor.startup(function () {
  loadFixture(Fixtures['challenges'], Challenges._collection);
  loadFixture(Fixtures['questions'], Questions._collection);
  loadFixture(Fixtures['products'], Products._collection);
  loadFixture(Fixtures['categories'], Categories._collection);
});
