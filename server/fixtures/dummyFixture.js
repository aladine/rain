// The "||" notation doesn't work yet
Fixtures = typeof Fixtures !== "undefined" ? Fixtures : {};

Fixtures.challenges = [
  { '_id': '8GSyahWnYGAoSGfh6', 'user_id' : 'uSJNh3ctCcGPNxTXo', 'name' : 'Javascript' },
  { '_id': '7GSyahWnYGAoSGfh6',  'user_id' : 'uSJNh3ctCcGPNxTXo', 'name' : 'PHP ' }
];

Fixtures.questions = [
  { 'challenge_id' : '8GSyahWnYGAoSGfh6', 'content' : 'How many continents on Earth', answers: ['3','4','5','6' ], correct_answer: 'A'},
  { 'challenge_id' : '8GSyahWnYGAoSGfh6', 'content' : 'How many types of patterns in Java', answers: ['A','B','C','D' ], correct_answer: 'A'},
  { 'challenge_id' : '8GSyahWnYGAoSGfh6', 'content' : 'Tell me more about yourself', answers: ['A','B','C','D' ], correct_answer: 'A'},
  { 'challenge_id' : '8GSyahWnYGAoSGfh6', 'content' : 'What is your motivation', answers: ['A','B','C','D' ], correct_answer: 'A'},
  { 'challenge_id' : '8GSyahWnYGAoSGfh6', 'content' : 'Why you apply to this job ', answers: ['A1','B1','C1','D1' ], correct_answer: 'A1' },
];


Fixtures.products = [
	{'name':'T-shirts', 'price': 120, 'unit': 'SGD'},
	{'name':'Macbook', 'price': 1200, 'unit': 'SGD'},
	{'name':'Watch', 'price': 100, 'unit': 'SGD'}
];


Fixtures.categories = [
	{'name':'Clothes'},
	{'name':'Electronics'},
	{'name':'Handicrafts'},
	{'name':Fake.word()}
];
// Fake.word
// Fake.sentence(3)
// Fake.paragraph(100)

